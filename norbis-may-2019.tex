\documentclass[10pt]{beamer}

% \usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{marvosym}
\usepackage{hyperref}
\usepackage{soul}
\usepackage{tikz}
\usepackage{wasysym}
%\usepackage{enumitem}

\usetikzlibrary{arrows,shapes.misc}

% setlist[itemize]{label={\footnotesize $\bullet$}, topsep=-0.2ex,
 %  partopsep=0pt, parsep=1pt, itemsep=0pt, leftmargin=3.5mm,
 %  labelsep=0.5ex} 

\newtheorem{thm}{Theorem}

\newcommand{\Rr}{\mathbb{R}}
\newcommand{\Nr}{\mathbb{N}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathcal{R}}
\newcommand{\Q}{\mathcal{Q}}
\renewcommand{\L}{\mathcal{L}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\G}{\mathcal{G}}
\newcommand{\Hg}{\mathcal{H}}
\newcommand{\V}{\mathcal{V}}
\newcommand{\E}{\mathcal{E}}
\newcommand{\pa}{\text{Pa}}
\newcommand{\D}{\mathcal{D}}

\newcommand{\Hnull}{\mathcal{H}_{\text{null}}}
\newcommand{\Halt}{\mathcal{H}_{\text{alt}}}
\newcommand{\PT}[1]{$\text{P}_\text{#1}$}

%\newcommand{\strong}[1]{{\color{red}#1}}
\newcommand{\balph}{\boldsymbol{\alpha}}
\DeclareMathOperator*{\argmax}{argmax}



\definecolor{frias}{rgb}{0,0.616,0.580}%{0,0.627,0.627}%{0,0.729,0.71}

\definecolor{uibr}{rgb}{0.811,0.235,0.227}
\definecolor{uibg}{rgb}{0.541,0.671,0.404}

\newcommand{\strong}[1]{{\color{uibr}#1}}

\newcommand{\chm}{{\large \strong{\smiley}}}
\newcommand{\crs}{{\large\color{red}\frownie}}

\usetheme{Malmoe}
\usecolortheme[named=uibr]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{}
\setbeamertemplate{enumerate subitem}{{\normalsize(\alph{enumii})}}

\graphicspath{{./}{../../Logos/}{../../Figures/}{../../2012/CRI-2012/}}

\title{\textcolor{black}{Network inference using transcriptomic data}}

\subtitle{\textcolor{black}{\\{NORBIS Course Computational Approaches in Transcriptome Analysis}}}

\author{Tom Michoel}

\date{7 May 2019}

\begin{document}

\begin{frame}
  \thispagestyle{empty}

  \vspace*{-7mm}
  \hspace*{-10mm}\includegraphics[width=\paperwidth]{uib-header}
  

  \titlepage

  
\end{frame}


\begin{frame}
  \frametitle{Data-driven biology}
  \fontsize{9}{11}\selectfont 
  \vspace*{-2mm}
  \begin{block}{}
    To understand how genetic, epigenetic and gene expression
    variation cause variation in health and disease traits in human,
    data-driven biology seeks to learn molecular mechanisms from
    interconnected omics data.
  \end{block}

  \vspace*{1mm}

  \begin{center}
    \includegraphics[width=.85\linewidth]{nrg3868-systemsgenomics}

    {\tiny [Ritchie et al, Nat Rev Genet 2015]}
  \end{center}
\end{frame}



\begin{frame}
  \frametitle{Gene expression determines cellular states}
  \begin{columns}
    \begin{column}{.4\linewidth}
      \begin{center}
        \includegraphics[width=\linewidth]{central_dogma}\\
        {\tiny [Wikipedia]}\\[3mm]

        \includegraphics[width=\linewidth]{paulsson1a}
      \end{center}
    \end{column}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item Genes are transcribed into mRNA and translated into protein.
      \item Every step in this process can be regulated by genetic or environmental factors.
      \item The repertoire and relative levels of proteins expressed determines cellular identity, fate, cell-to-cell communication, etc.
      \item Variation in cellular properties causes variation in phenotypes.
      \end{itemize}
    \end{column}
  \end{columns}
  \bigskip
  \begin{center}
    Genetic variation causes  phenotypic variation by affecting gene expression.\\[3mm]

    \strong{How does genetic variation affect mRNA expression?}
    
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Genetic variation causes variation in transcription
    factor binding}
  \begin{itemize}
  \item TFs bind to accessible regulatory DNA elements to control gene transcription. 
  \item Environmental perturbations affect TF concentrations in nucleus.
  \item Genetic perturbations affect ability of TFs to bind to DNA.
  \end{itemize}
  \begin{center}
    \includegraphics[width=.75\linewidth]{albert2016-fig1B-2}\\
     {\tiny [Albert and Kruglyak, Nat Rev Genet (2016)]}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Genes are organized in hierarchical, multi-tissue causal networks}
  \fontsize{9}{10.5}\selectfont
  \begin{columns}
    \begin{column}{.5\linewidth}
      \begin{center}
      \includegraphics[width=\linewidth]{civelek_nrg_fig3a}\\
      {\tiny [Civelek and Lusis, Nat Rev Genet (2014)]}

      \vspace*{5mm}

      \includegraphics[width=\linewidth]{albert2016-fig3A}\\
       {\tiny [Albert and Kruglyak, Nat Rev Genet (2016)]}
    \end{center}
    \end{column}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item Variation in expression of one gene has downstream
        consequences on expression of other genes.
      \item Example: Introduction of just 4 TFs (``Yamanaka factors'')
        converts adult cells into stem cells.
      \item Hundreds to thousands of genes are differentially expressed between cellular states (e.g.\ healthy vs.\ disease).
      \item Gene expression in one tissue can affect gene expression
        in other tissues.
      \item Phenotype variation also causes gene expression variation
        (``environmental'' perturbation).
      \end{itemize}
    \end{column}
  \end{columns}
  \fontsize{10}{12}\selectfont
  \medskip
  \begin{center}
    \strong{Inference (reconstruction) of causal gene networks is
      essential to understand how the genotype determines the
      phenotype.}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Gene regulatory network inference from transcriptomic
    data}
  \begin{columns}
    \begin{column}{0.7\linewidth}
      \begin{center}
        \includegraphics[width=\linewidth]{fig_gardner2005}\\
        {\tiny [Gardner and Faith, Phys. Life Rev. (2005)]}  

        \medskip

        \includegraphics[width=\linewidth]{bansal2007_fig2}\\
        {\tiny [Bansal \emph{et al}, Mol. Syst. Biol. (2007)]}  
      \end{center}
    \end{column}
    \begin{column}{0.3\linewidth}
      \begin{center}
        \includegraphics[width=\linewidth]{nrg2612-f3-ac}\\
        {\tiny [Mackay \emph{et al}, Nat. Rev. Genet (2009)]}    
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{From correlation to causation}
  \begin{itemize}
  \item The aim of network inference is to reconstruct
    context-specific \textbf{causal} gene regulatory networks.\\[2mm]
  \item To infer causality from correlation, additional data is required:
    \begin{itemize}
    \item Prior information (e.g. gene annotation).
    \item Instrumental variables (e.g. genetic markers).
    \item Temporal data
    \end{itemize}

    \medskip

    \begin{center}
      \includegraphics[width=\linewidth]{fig-causal}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Module network inference learns regulatory programs that
    ``explain'' coexpression modules}
  \begin{center}
    \includegraphics[width=\linewidth]{friedman2004_fig3}\\
    {\tiny [Friedman, Science (2004)]}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Module network inference from cancer gene expression
    data}
  \begin{columns}
  \begin{column}[t]{0.55\linewidth}
    \begin{minipage}[t]{.3\linewidth}
      \vspace*{-40mm}
      \hspace*{-2mm}    \includegraphics[width=1.2\linewidth]{erbon_figure4a}
      \begin{center}
        \hspace*{2mm}\includegraphics[width=0.8\linewidth]{erbon_fig_zeb}     
      \end{center}
    \end{minipage}%
    \begin{minipage}[t]{.7\linewidth}
      \hspace*{4mm}\includegraphics[width=\linewidth]{erbon_figure3}
    \end{minipage}

    \vspace*{-4mm}
    \begin{center}
      {\tiny [Bonnet et al, PLOS One 2010]}   
    \end{center}
   

    \fontsize{8}{9}\selectfont
    \begin{itemize}
    \item microRNA regulated modules predicted from pan-cancer data;
      experimentally validated in breast cancer cell line.
    \item Top-predicted copy number alterations regulating TCGA
      glioblastoma modules involved in canonical glioblastome
      signaling pathways and associated to significant survival
      differences.
    \end{itemize}

    \end{column}
    % \begin{column}[t]{0.35\linewidth}
    %   \hspace*{4mm}\includegraphics[width=\linewidth]{erbon_figure3}
    % \end{column}
    \begin{column}[t]{.5\linewidth}
      \includegraphics[width=\linewidth]{pathway_eb}

      \includegraphics[width=\linewidth]{survival_graph}

      \vspace*{-4mm}
      \begin{center}
        {\tiny [Bonnet,Calzone,\textbf{Michoel}, PLOS Comp Biol 2015]}
      \end{center}
    \end{column}
  \end{columns}
  
\end{frame}

\begin{frame}
  \frametitle{Module network inference software}
  \vspace*{-6mm}
  \hspace*{67mm}\fbox{\scriptsize\url{https://github.com/eb00/lemon-tree}}
  \fontsize{9}{10}\selectfont
  \begin{columns}
    \begin{column}{0.8\linewidth}
     \begin{itemize}
     \item Probabilistic graphical model where:
       \begin{itemize}
       \item Genes belonging to the same module share the same
         parameters.
       \item Conditional distributions are modelled by decision trees
         with parents at internal nodes and Gaussian distributions at
         the leaves.
       \end{itemize}
      \item Hidden variables decouple module assignment and leaf
        clustering from regulator assignment.
      \item Gibbs sampling and ensemble averaging for  robust two-way
        clustering of expression data and regulator identification.
      \end{itemize}
    \end{column}
    \begin{column}{0.21\linewidth}
      \includegraphics[width=\linewidth]{segal_fig2_sub}\\
      {\tiny [Segal et al, Nat Genet 2003]}
    \end{column}
  \end{columns}

  \begin{columns}
    \begin{column}{0.51\linewidth}
      %\vspace*{3mm}
      \begin{itemize}
      \item Command line software organized as independent ``tasks''.
      \item (Some) support for parallel processing.
      \item Results stored in XML and text formats $+$ automatic
        generation of figures and GO enrichment tables.
      \end{itemize}
      \begin{center}
        {\tiny [Joshi,\dots,\textbf{Michoel}, Bioinformatics 2008,
          2009]\\

          [Bonnet,Calzone,\textbf{Michoel}, PLOS Comp Biol 2015]}
      \end{center}
    \end{column}
    \begin{column}{.5\linewidth}
      \vspace*{-2mm}
      \includegraphics[width=\linewidth]{l3_flow}
    \end{column}
  \end{columns}
 \end{frame}


\begin{frame}
  \frametitle{From correlation to causation via ``Mendelian
    randomization''}
  \begin{itemize}
  \item Genetic variation between individuals precedes gene expression
    variation $\Rightarrow$ SNPs act as instrumental variables to
    infer causal direction between correlated expression traits.
  \item Molecular version of a random trial.

    \bigskip

    \begin{center}
      \includegraphics[width=0.7\linewidth]{civelek_nrg_fig3a}\\
      {\tiny [Civelek and Lusis, Nat Rev Genet (2015)]}
      
      % \vspace*{15mm}

    \bigskip
    
    \includegraphics[width=\linewidth]{rockman2008_fig2}\\
    {\tiny [Rockman, Nature (2008)]}
   \end{center}
  \end{itemize}

 
\end{frame}


\begin{frame}
  \frametitle{Genome-wide transcriptome variation studies map the
    genetic architecture of gene expression}
  \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}{.3\linewidth}
      \includegraphics[width=\linewidth]{GTEx_diagram}

      \bigskip

      \includegraphics[width=\linewidth]{nrg2969-f1}
    \end{column}
    \begin{column}{.79\linewidth}
      \begin{itemize}
      \item eQTL $=$ expression QTL $=$ Genetic variant associated
        with gene expression level.
      \item Strongest effects in gene proximity (regulatory region).
      \item RNA-sequencing measures expression levels of $\sim$40k
        transcripts in a single sample.
      \item Gene expression is highly tissue-specific, but only few
        tissues are easily accessible (blood, immune cells).
      \item Genotype-Tissue Expression project (GTEx): 48 tissues, 620
        donors (deceased), 10,294 samples $\to$ 341,316 eGenes (31,403
        unique)
      \end{itemize}

      \vspace*{5mm}

      \includegraphics[width=\linewidth]{GTEx_tissues_2.png}
      \begin{center}
        {\tiny [GTEx website (2017)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Causal inference using instrumental variables}
  \fontsize{9}{11}\selectfont 
  \begin{center}
    \includegraphics[width=.45\linewidth]{civelek_nrg_fig3a}\\[5mm]
    \fontsize{7}{11}\selectfont
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_1$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (X) edge (Y);
  \end{tikzpicture} \quad\textit{vs.}\quad
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_2$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (Y) edge (X);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (.5,1) {$M_3$};
    \node (X) at (1,0.5) {$A$};
    \node (Y) at (1,-0.5) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (E) edge (Y);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_4$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge[out=30,in=150] (Y);
    \path[->,thick] (Y) edge (X);
  \end{tikzpicture}
  \end{center}
  \begin{enumerate}
  \item \strong{\textit{Trans-association:}} Statistical dependence
    between $E_A$ and $B$ excludes model $M_2$.\\[2mm]
  \item \strong{\textit{Mediation:}} Statistical independence between
    $E_A$ and $B$ conditioned on $A$ excludes model $M_3$ and $M_4$.
  \end{enumerate}
  \begin{center}
    \strong{BUT}

    \bigskip

    Mediation fails in the presence of hidden confounders due to
    collider effect.
  \end{center}
  
  \begin{center}
    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=2mm,scale=.8]
      \node[shape=circle,inner sep=1pt,draw] (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick] (X) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\;$\Rightarrow$\;
    \begin{tikzpicture}[baseline=2mm,scale=.8]
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[-,thick,dashed] (H) edge (E);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Causal inference with hidden confounders}
  \fontsize{9}{11}\selectfont 
  \begin{center}
    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_1$};
      \node (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick] (X) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\quad\textit{vs.}\quad
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_2$};
      \node (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\; or\;
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_3$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \node (H) at (1.5,1) {$H$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (Y) edge (X);
    \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (.5,2) {$M_4$};
    \node (X) at (1,0.5) {$A$};
    \node (Y) at (1,-0.5) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (E) edge (Y);
  \end{tikzpicture}
  \end{center}
  \begin{enumerate}
  \item \strong{\textit{Trans-association:}} Statistical dependence
    between $E_A$ and $B$ excludes model $M_2$ and $M_3$.\\[2mm]
  \item \strong{\textit{Non-independence:}} Statistical dependence
    between $A$ and $B$ conditioned on $E_A$ excludes model $M_4$.
  \end{enumerate}
  \vspace*{-5mm}
  \begin{center}
    \strong{BUT}
    
    \smallskip
    
    Non-independence fails if there is a hidden confounder in model
    $M_4$.\\[2mm]

    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node (X) at (1,0.5) {$A$};
      \node (Y) at (1,-0.5) {$B$};
      \node (E) at (0,0) {$E_A$};
      \node (H) at (2,0) {$H$};
      
      \path[->,thick] (E) edge (X);
      \path[->,thick] (E) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\\[2mm]

    \textit{Because enhancer-gene interactions are \textbf{local}, a
      direct effect of $E_A$ on $B$ can only occur if $A$ and $B$ are
      co-located on the genome.}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Findr software for causal network inference}
 \fontsize{9}{11}\selectfont 

  \begin{columns}
    \begin{column}{.58\linewidth}
      \vspace*{-4mm}
      \begin{itemize}
      \item Findr software implements 6 likelihood-ratio tests and outputs probabilities (1-FDR) of hypotheses being true.\\[2mm]
      \item Analytic results to avoid random permutations, resulting in massive speed-up.\\[2mm]
      \item Combining LLR tests gives probabilities of causal effects:\\%[1mm]
        \begin{itemize}
        \item \textbf{Mediation}: %\\[1mm] 
          $P = P_1\times P_2\times P_3$\\[1mm]
        \item \textbf{Non-independent \textit{trans}-effect}: \\[1mm] 
          $P = P_1\times P_2 \times P_5$
        \end{itemize}
      \end{itemize}
     
    \end{column}
    \begin{column}{.49\linewidth}
      \begin{center}
        \includegraphics[width=.7\linewidth]{civelek_nrg_fig3a}
      \end{center}
      \includegraphics[width=\linewidth]{findr-tests} 
    \end{column}
  \end{columns}

  \vspace*{-6mm}
  \begin{columns}
    \begin{column}{.2\linewidth}
      \begin{center}
        \includegraphics[width=.7\linewidth]{lingfei_wang}\\
        {\footnotesize Lingfei Wang}
      \end{center}
    \end{column}
    \begin{column}{.8\linewidth}
      \begin{center}
        \href{https://github.com/lingfeiwang/findr}{{\small \texttt{https://github.com/lingfeiwang/findr}}}\\[2mm]
        \footnotesize[Wang \& Michoel, PLOS Comp Biol (2017)]
      \end{center}
    \end{column}
  \end{columns}
  
\end{frame}

\begin{frame}
  \frametitle{Findr accounts for weak secondary linkage, allows for
     hidden confounders, and outperforms existing methods} 
   \fontsize{9}{10.5}\selectfont 
   \begin{center}
     \includegraphics[width=.55\linewidth]{fig1-DREAM}
   \end{center}
   \vspace*{-2mm}
   \begin{itemize}
   \item \textbf{Mediation:} high specificity, but low sensitivity due
     to hidden confounders, worse with increasing sample size.
   \item \textbf{Non-independent effects:} much higher sensitivity at
     potential cost of increase in false positives.
   \end{itemize}

    %\smallskip

    \begin{center}
      \fbox{
        \begin{minipage}{.9\linewidth}
          \begin{center}
            In simulations and comparisons to known miRNA and
            TF-targets, ``non-independent effects'' always performed better 
            than ``mediation''.  
          \end{center}
        \end{minipage}
      }
    \end{center}

 \end{frame}

\begin{frame}
  \frametitle{The STAGE and STARNET studies}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \fontsize{9}{11}\selectfont 

      Multi-organ expression profiling in 7 vascular and metabolic
      tissues of patients undergoing coronary artery bypass grafting
      surgery at Karolinska University Hospital/Tartu University
      Hospital\\[3mm]

      \textbf{STAGE}
      \begin{itemize}
      \item 109 individuals, SNP arrays
      \item 612 tissue expression arrays
      \end{itemize}
      {\fontsize{7}{9}\selectfont [Foroughi-Asl \textit{et al}, Circ
        Cardiovasc Genet 2015]}\\[3mm]
    
      \textbf{STARNET}
      \begin{itemize}
      \item 566 individuals, SNP arrays
      \item 3,786 tissue RNA-seq profiles
      \end{itemize}
      \qquad{\fontsize{7}{9}\selectfont [Franz\'en \textit{et al},
        Science 2016]}
    \end{column}
    \begin{column}{.58\linewidth}
      \includegraphics[width=\linewidth]{aad6970-TableS1}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Gene expression profiles and eSNPs reflect tissue
    characteristics}
  \begin{columns}
    \begin{column}{0.7\linewidth}
      \fontsize{9}{10}\selectfont
      \begin{itemize}
      \item 29,530 \textit{cis}-eSNPs for 6,450 genes, $\sim 75\%$ are
        tissue-specific.
      \item Shared eSNPs: closer to TSS and stronger effect.
      \item More overlap between similar tissues.
      \end{itemize}
      \begin{center}
        \vspace*{-2mm}
        \hspace*{-4mm}\includegraphics[width=1.1\linewidth]{hassan-Figure1}

        {\tiny [Foroughi
          Asl,\dots,\textbf{Michoel$^\ast$},Bj\"orkegren$^\ast$, Circ
          Cardiovas Genet 2015]}
      \end{center}
    \end{column}
    \begin{column}{0.3\linewidth}
      \begin{center}
        \vspace*{-4mm}
        {\tiny Tissue-specific eSNP example}\\
        \includegraphics[width=\linewidth]{SORT1_vs_rs599839}

        \smallskip

        {\tiny Tissue-shared eSNP example}\\
        \includegraphics[width=\linewidth]{HLADOA1_vs_rs9272346}  
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Multi-tissue eSNPs carry more inherited CAD risk}
  \begin{itemize}
  \item Disease SNP $=$ SNP with GWAS case/control $P<0.05$.
  \item Risk enrichment $=$ More disease SNPs than expected by chance.
  \end{itemize}

  \begin{center}
    \includegraphics[width=0.75\linewidth]{hassan-Figure2}

     {\tiny [Foroughi
          Asl,\dots,\textbf{Michoel$^\ast$},Bj\"orkegren$^\ast$, Circ
          Cardiovas Genet 2015]}
  \end{center}
\end{frame}

\begin{frame}
  

  \frametitle{STAGE eQTLs link genotypes predictive of CAD to
    tissue-specific and cross-tissue gene networks}
\fontsize{8}{10}\selectfont 
  \begin{columns}
    \begin{column}{.5\linewidth}
      \vspace*{-4mm}
      \hspace*{-10mm}
      \begin{itemize}
      
      \item Multi-tissue clustering identified 171 co-expression
        clusters (94 tissue-specific/77 cross-tissue).
      \item 61 clusters associated to key CAD phenotypes (athero,
        cholesterol, glucose, CRP).
      \item 30 CAD-causal clusters (CAD risk enriched eSNPs/GWAS
        genes).
      \item 12 clusters conserved in mouse with same phenotype
        association in same tissue.
      \item Key drivers of athero-causal Bayesian gene networks
        validated by siRNA targeting in THP-1 foam cells.
      \end{itemize}
    \end{column}
    \begin{column}{.65\linewidth}
     

      \includegraphics[width=\linewidth]{Figure_1_Study_Overview}

     % \vspace*{-4mm}
      \begin{center}
        {\tiny [Talukdar \textit{et al}, Cell Systems (2016)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Network inference predicts CAD-causal networks}
    \fontsize{8}{10}\selectfont 

    \vspace*{-10mm}
    \hspace*{.95\linewidth}\begin{minipage}{.12\linewidth}
      \includegraphics[width=\linewidth]{fig-causal-prior}
    \end{minipage}

    \smallskip

    \begin{columns}
    \begin{column}[t]{0.5\linewidth}
      \textbf{1.} AAW-specific cluster of RNA processing genes
      associates with stenosis score.
    \begin{center}
      \includegraphics[width=\linewidth]{BN42_human}
    \end{center}

    \medskip

    \textbf{2.} Coexpression is conserved in aorta of atherosclerotic mice and
    associates with aortic lesion size.
    \begin{center}
      \includegraphics[width=\linewidth]{BN42_mouse}  
    \end{center}
    \end{column}
    \begin{column}[t]{0.5\linewidth}
      \textbf{3.} siRNA KO of 4/7 key driver genes has significant
      effect in cell line model of atherosclerosis.
      \begin{center}
        \includegraphics[width=\linewidth]{Husain_fig3g.pdf}
      \end{center}

      \textbf{4.} Significant number of targets show differential
      expression upon siRNA KO of regulators.
    \end{column}
  \end{columns}

\end{frame}

\begin{frame}
  \frametitle{Network inference predicts CAD-causal networks}

  \fontsize{9}{11}\selectfont 


  \begin{minipage}{.85\linewidth}
    Network predicted with Findr in \textbf{foam cells} -- type of
    macrophage, ingest LDL on blood vessel walls, involved in
    atherosclerotic plaque growth and inflammation.
  \end{minipage}\hspace*{8mm}\begin{minipage}{.15\linewidth}
    \includegraphics[width=\linewidth]{fig-causal-snp}
  \end{minipage}
  

  \vspace*{-3mm}
  
  \hspace*{-7mm} \begin{minipage}{.6\linewidth}
    \includegraphics[width=\linewidth]{LDLR_net_2}
    \end{minipage}~%
    \hspace*{-3mm} \begin{minipage}{.52\linewidth}
      \begin{itemize}
      \item \textit{LDLR} -- LDL receptor; critical role in regulating
        blood cholesterol level.\\[2mm]
      \item \textit{HMGCR} -- rate-limiting enzyme in cholesterol
        biosynthesis; inhibited by statins. \\[2mm]
      \item \textit{MMAB} -- enzyme involved in vitamin B12
        metabolism; necessary for function of MCM (\textit{MUT}),
        which helps break down cholesterol. \\[2mm]
      \item 21/34 genes involved in cholesterol or lipid metabolism
        ($p=10^{-17}$).
      \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Functional dissection of a GWAS locus for plasma cortisol}
  \begin{columns}
    \begin{column}{.7\linewidth}
      \begin{itemize}
      \item Elevated levels of plasma cortisol contribute to risk
        factors for CVD (hypertension, obesity, diabetes).
      \item GWAMA in 25,000 individuals has identified a single locus
        spanning \emph{SERPINA6/A1} at genome-wide significance.
      \item \emph{SERPINA6} codes for CBG, cortisol binding globulin,
        which is produced in the liver, binds to cortisol in blood for
        transport  to other tissues.
      \item Cortisol in tissues binds to GR, glucocorticoid receptor,
        a TF expressed in almost every cell, regulating developmental,
        metabolic and immune response genes.
      \end{itemize}
    \end{column}
    \begin{column}{.35\linewidth}
      \begin{center}
        \includegraphics[width=.8\linewidth]{CORNET-manhattan}\\
        {\tiny [Bolton \emph{et al}. PLOS Genet (2014)]}
        
        \medskip
        
        \includegraphics[width=\linewidth]{LZ-CORNET-GWAMA.png}\\
        {\tiny [Crawford, Walker. Unpublished data.]}
      \end{center}
      
    \end{column}
  \end{columns}
  \bigskip
  \begin{center}
    \fbox{
      \begin{minipage}{.9\linewidth}
        \begin{center}
          What are the tissue-specific consequences and effects on
          disease of genetic variation for plasma cortisol?
        \end{center}
      \end{minipage}
    }
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{\textit{Cis} and \textit{trans} effects of genetic
    variation for plasma cortisol}
   \fontsize{9}{11}\selectfont 
  \begin{columns}
    \begin{column}{.6\linewidth}
      \vspace*{2mm}
      \begin{itemize}
      \item GWAMA peak for cortisol overlaps with
        \textit{cis}-regulatory variants (eQTL) for \emph{SERPINA6} in
        liver.
      \item \emph{Trans} effects on gene expression observed in liver
        and adipose tissues.
      \item Filtered significant \emph{trans} genes for potential mediators
        of cortisol-responsive effects:
        \begin{itemize}
        \item GR ChIP-seq in adipocytes.
        \item Glucocorticoid response element in promoter.
        \item Differentially expressed upon dexamethasone treatment in
          mouse adipose.
        \end{itemize}
      \item Found 14 (liver), 13 (VAF), 11 (SF) candidates.
      \end{itemize}
      
      \end{column}
    \begin{column}{.4\linewidth}
      \vspace*{-10mm}
      \includegraphics[width=\linewidth]{LZ-CORNET-STARNET-Liver-SERPINA6.png}
    \end{column}
  \end{columns}


  \hspace*{57mm}
  \begin{minipage}{.5\linewidth}
    \vspace*{-10mm}
    \begin{flushright}
      \includegraphics[width=.65\linewidth]{CORNET_cis07_trans_075}\hfill
      \begin{minipage}{.25\linewidth}
        \vspace*{-20mm}
        \begin{center}
          \includegraphics[width=\linewidth]{sean_bankier}\\
          {\tiny Sean Bankier}    
        \end{center}
      \end{minipage}
      
    \end{flushright}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Network inference predicts a cortisol-responsive\\ gene
    network in adipose tissue controlled by \emph{IRF2}}

  \fontsize{9}{11}\selectfont 

  \vspace*{-12mm}
  \hspace*{.93\linewidth}\begin{minipage}{.15\linewidth}
    \includegraphics[width=\linewidth]{fig-causal-snp}
  \end{minipage}

  \vspace*{-8mm}

  \begin{columns}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item Subcutaneous adipose expression of the TF \emph{IRF2} is
        associated with genetic variation for cortisol.
      \item \emph{IRF2} is bound by GR in adipose ChIP-seq data.
      \item Causal inference predicts that \emph{IRF2} regulates an adipose
        gene network.
      \item Predicted targets are enriched for GREs and IRF-family
        binding motifs.
      \end{itemize}
      \medskip
      
      \begin{minipage}{.2\linewidth}
        \begin{center}
          \includegraphics[width=\linewidth]{sean_bankier}\\
          {\tiny Sean Bankier}    
        \end{center}
      \end{minipage}\qquad
      \begin{minipage}{.65\linewidth}
        \includegraphics[width=\linewidth]{STARNET-IRF2-boxplot}  
      \end{minipage}
    \end{column}
    \begin{column}{.4\linewidth}
                  
      %\vspace*{-5mm}

      \hspace*{-9mm}\includegraphics[width=1.35\linewidth]{IRF2-net}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Take home messages}
  \begin{itemize}
  \item The aim of network inference is to reconstruct
    context-specific \textbf{causal} gene regulatory networks from
    large-scale transcriptomics data.%\\[2mm]
  \item To infer causality from correlation, additional data is required:
    \begin{itemize}
    \item Prior information (e.g. gene annotation).
    \item Instrumental variables (e.g. SNP markers).
    \item Temporal data
    \end{itemize}
    \begin{center}
      \includegraphics[width=.7\linewidth]{fig-causal}
    \end{center}
  \item Biomedical studies with complex experimental designs generate
    large multi-omics datasets. Network inference helps to solve
    concrete problems, but no off the shelf solutions.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Acknowledgements}
  \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}[t]{.33\linewidth}
      \begin{block}{Group members}
        \textbf{Sean Bankier}\\
        Pau Erola\\
        Tor Helleland\\
        Siddharth Jayaraman\\
        Ammar Malik\\
       \textbf{Lingfei Wang}\\     
      \end{block}
      \begin{block}{Edinburgh}
        Ruth Andrew\\
        Andy Crawford\\
        Filippo Menolascina\\
        Brian Walker\\
      \end{block}
    \end{column}
    \begin{column}[t]{.33\linewidth}
      \begin{block}{CNRGH}
        \textbf{Eric Bonnet}
      \end{block}
      \begin{block}{Ghent University}
        Pieter Audenaert
      \end{block}
      \begin{block}{ISMMS/Karolinska}
        Ariella Cohain\\        
        \textbf{Hassan Foroughi Asl}\\
        Oscar Franz\'en\\
        \textbf{Husain Talukdar}\\
        Eric Schadt\\
        \textbf{Johan Bj\"orkegren}\\
      \end{block}
    \end{column}
    \begin{column}[t]{.34\linewidth}
      \vspace*{-10mm}

      \hspace*{16mm}\begin{minipage}{.6\linewidth}
        \includegraphics[width=\linewidth]{UiB-emblem_gray}
      \end{minipage}

      \begin{flushright}
        
        \includegraphics[width=.35\linewidth]{British_Heart_Foundation}\\%[5mm]

        \includegraphics[width=.7\linewidth]{mrc}\\[2mm]
        
        \includegraphics[width=\linewidth]{bbsrc-colour}\\[2mm]

        \hspace*{-21mm}\includegraphics[height=.9cm]{NIH}\\[5mm]

        
      \end{flushright}
      \hspace*{-11mm}\textbf{{\large http://lab.michoel.info}}
    \end{column}
  \end{columns}
  \vspace*{-18mm}
  \hspace*{-4mm}\begin{minipage}{.27\linewidth}
    \includegraphics[width=\linewidth]{Cbu_logo_black_transparent}
  \end{minipage}
  
  %\vspace*{2mm}

  % \begin{center}
  %   \textbf{{\large http://lab.michoel.info}}
  % \end{center}
\end{frame}

% \begin{frame}
%   \frametitle{Acknowledgements}

%   \fontsize{9}{11}\selectfont

%   \vspace*{-6mm}

%   \begin{columns}
%     \begin{column}[t]{.3\linewidth}
%       \begin{block}{Group members}
%         \textbf{Sean Bankier}\\
%         Pau Erola\\
%         Siddharth Jayaraman\\
%        \textbf{Lingfei Wang}\\     
%       \end{block}
%       \begin{block}{CNRGH}
%         \textbf{Eric Bonnet}
%       \end{block}
%       \begin{block}{Edinburgh}
%         Ruth Andrew\\
%         Brian Walker\\
%       \end{block}
%       \vspace*{-4mm}
%       \begin{center}
%         \textbf{{\large http://lab.michoel.info}}
%       \end{center}
%     \end{column}
%     \begin{column}[t]{.3\linewidth}
%       \begin{block}{ISMMS/Karolinska}
%         Ariella Cohain\\        
%         Hassan Foroughi Asl\\
%         \textbf{Oscar Franz\'en}\\
%         \textbf{Husain Talukdar}
%         Eric Schadt\\
%         \textbf{Johan Bj\"orkegren}\\
%       \end{block}
%       \begin{block}{Ghent University}
%         Pieter Audenaert
%       \end{block}

      
%     \end{column}
%     \begin{column}[t]{.4\linewidth}
%       \vspace*{-7mm}

%       \hspace*{22mm}\begin{minipage}{.5\linewidth}
%         \includegraphics[width=\linewidth]{UiB-emblem_gray}
%       \end{minipage}

%       \begin{flushright}
%         \begin{minipage}{.7\linewidth}
%           \includegraphics[width=\linewidth]{Cbu_logo_black_transparent}
%         \end{minipage}
%         % \begin{minipage}{.3\linewidth}
%         %   \includegraphics[width=\linewidth]{British_Heart_Foundation}
%         % \end{minipage}
%         \\[5mm]  
        

%         \includegraphics[width=.9\linewidth]{bbsrc-colour}\\[6mm]

%         \hspace*{-35mm}\includegraphics[height=.9cm]{NIH}\quad
%         \includegraphics[width=.3\linewidth]{British_Heart_Foundation}

%       \end{flushright}
%     \end{column}
%   \end{columns}
  
%   \end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
